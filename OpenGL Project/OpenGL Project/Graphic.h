#pragma once
#include "Camera.h"
#include "Shader.h"
#include "Player.h"
#include "Entity.h"
#include "Model.h"


#include "irrKlang.h"
using namespace irrklang;
int sheepSound = 150;
ISoundEngine *SoundEngine = createIrrKlangDevice();
Camera *pCamera = nullptr;
int choice = 0;

class Graphic
{
private:
	/* Settings */
	unsigned int SCR_WIDTH = 800, SCR_HEIGHT = 600;
	const unsigned int SHADOW_WIDTH = 4096, SHADOW_HEIGHT = 4096;

	/* Timing */
	double deltaTime = 0.0f;	// Time between current frame and last frame
	double lastFrame = 0.0f;

public:
	Graphic() = default;
	~Graphic() = default;

	/* Main Draw Method */
	void draw(std::string strExePath);

private:
	/* Initialization Methods */
	void initializeGLFW();
	void initializeWindow();
	void initializeGLEW();
	void initializeTextures();
	void initializeShaders();
	void initializeObjects();

	/* Configurations */
	void configureDepthMapFBO();
	void configureObjects();

	/* Creation Methods */
	unsigned int loadTexture(const std::string& strTexturePath);
	unsigned int loadSkyboxTexture(std::vector<std::string> faces);

	/* Render Methods */
	void renderPlayer(Shader& shader, glm::mat4& model);
	void renderScene();
	void renderScene(Shader& shader);
	void renderDepthOfSceneToTexture();
	void renderGeneratedDepthShadowMap();
	void renderShadows();

	void renderCube();
	void renderFloor();
	void renderGrass(const Shader& shaderBlending, glm::mat4& model);
	void renderBunchOfGrass(const Shader& shaderBlending, glm::mat4&model);

	/* Helper Methods */
	void setWindowedFullscreen();
	void processInput(GLFWwindow *window);
	void cleanup();
	void setCurrentShader(Shader& shader);
	void setCurrentSkyboxShader(Shader& shader);
	void setCurrentTexture(const unsigned int& textureID);
	void prepareShader(const unsigned int textureID, Shader& shader, glm::mat4 & model);
	void prepareSkyboxShader(const unsigned int textureID, Shader& shader, glm::mat4& model);
	void frameTimeLogic();
	bool collision(glm::vec3 a, glm::vec3 aSize, glm::vec3 b, glm::vec3 bSize);

	void loadModels();

	/* GLFW Callbacks: whenever the window size changed (by OS or user resize) this callback functions executes */
	static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
	static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
	static void scroll_callback(GLFWwindow* window, double xoffset, double yOffset);


private:
	/* Path to the current Exe file */
	std::string strExePath;

	/* Main Window */
	GLFWwindow * window;

	/* Textures */
	std::map<TextureID, unsigned int> textures;

	/* Scene Objects */
	std::vector<Entity> objects;

	/* Shaders */
	Shader shadowMappingShader, shaderBlending, shadowMappingDepthShader, skyboxShader, objShader;
	unsigned int depthMapFBO;
	unsigned int depthMap;

	/* Lighting info */
	glm::vec3 lightPos;
	glm::mat4 lightSpaceMatrix;

	std::vector<Model> models;

	float textureBrightness;

	bool first;

	float moveRotate;
};





























//=================================================================================//
//								   FUNCTIONS BODY								   //
//=================================================================================//


inline void Graphic::loadModels()
{
	Model ourModel12("sheep/sheep.obj");
	this->models.push_back(ourModel12);  //good  0

	Model ourModel13("castle/Castle OBJ.obj");
	this->models.push_back(ourModel13); //good 1

	Model woods("woods/woods.obj");
	this->models.push_back(woods); //good  2

	Model cannon("cannon/14054_Pirate_Ship_Cannon_on_Cart_v1_l3.obj");
	this->models.push_back(cannon); //good  3

	Model house("house/medieval house.obj");
	this->models.push_back(house);  //good  4

	Model duck("duck/duck.obj"); //good  5
	this->models.push_back(duck);

	Model cabin("woodenCabin/cabin.obj");
	this->models.push_back(cabin); //good  6

	Model rock("rock/rock.obj");
	this->models.push_back(rock); //good 7

	Model farm("farm/farm.obj");
	this->models.push_back(farm); //good 8

	Model horse("horse/horse.obj");
	this->models.push_back(horse);  //9

	Model wel("well/old_well.obj");
	this->models.push_back(wel);  //10

	Model castle("castle/Castle OBJ.obj");
	this->models.push_back(castle);    //11

	Model cat("cat/cat.obj");
	this->models.push_back(cat); //12

	Model treeModel("tree/Tree.obj");
	this->models.push_back(treeModel);
	system("CLS");
}


/* Main Draw Method */
inline void Graphic::draw(std::string strExePath)
{
	/* Set the Exe Path from the class so we can acces in any methods */
	this->strExePath = strExePath;

	/* Initialisations */
	initializeGLFW();
	initializeWindow();
	initializeGLEW();
	initializeTextures();
	initializeShaders();
	initializeObjects();
	loadModels();

	/* Configurations */
	//configureDepthMapFBO();

	/* Configure depth map FBO */
	glGenFramebuffers(1, &depthMapFBO);

	/* Create depth texture */
	configureDepthMapFBO();

	configureObjects();

	/* GL configurations */
	glEnable(GL_CULL_FACE);

	first = true;
	moveRotate = 0.0f;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/* Render loop */
	while (!glfwWindowShouldClose(window)) {
		frameTimeLogic();

		processInput(window);
		renderShadows();

		renderScene();

		/* GLFW: swap buffers and poll Input/Output events (keys pressed/released, mouse moved etc.) */
		glfwSwapBuffers(window);
		glfwPollEvents();
		std::cout << "FPS: "<<(int)(1 / deltaTime)<<std::endl;
	}
	cleanup();

	/* GLFW: terminate, clearing all previously allocated GLFW resources */
	glfwTerminate();
}


/* Initialization Methods */
inline void Graphic::initializeGLFW()
{
	/* GLFW: initialize and configure */
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
}

inline void Graphic::initializeWindow()
{
	setWindowedFullscreen();

	/* GLFW window creation */
	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Dimension Travel", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, Graphic::framebuffer_size_callback);
	glfwSetCursorPosCallback(window, Graphic::mouse_callback);
	glfwSetScrollCallback(window, Graphic::scroll_callback);

	/* Tell GLFW to capture our mouse */
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

inline void Graphic::initializeGLEW()
{
	glewInit();
	glEnable(GL_DEPTH_TEST);

}

inline void Graphic::initializeTextures()
{
	textures[TextureID::DIRT] = loadTexture(strExePath + "\\Dirt.jpg");
	textures[TextureID::GRASS] = loadTexture(strExePath + "\\Grass.jpg");
	textures[TextureID::GRAVEL] = loadTexture(strExePath + "\\Gravel.jpg");
	textures[TextureID::MUD] = loadTexture(strExePath + "\\Mud.png");
	textures[TextureID::BRICK] = loadTexture(strExePath + "\\Brick.jpg");

	textures[TextureID::GRASS_BUSH] = loadTexture(strExePath + "\\GrassBush.png");

	textures[TextureID::BLENDMAP] = loadTexture(strExePath + "\\BlendMap.png");
	textures[TextureID::BLENDMAP_DIFFUSE] = loadTexture(strExePath + "\\BlendMapDiffuseTexture.png");

	std::vector<std::string> skyboxFaces;
	std::string version = "v2";
	skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + version + "\\right.jpg");
	skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + version + "\\left.jpg");
	skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + version + "\\top.jpg");
	skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + version + "\\bottom.jpg");
	skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + version + "\\front.jpg");
	skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + version + "\\back.jpg");
	textures[TextureID::SKYBOX] = loadSkyboxTexture(skyboxFaces);
}

inline void Graphic::initializeShaders()
{
	/* Shaders */
	static Shader shadowMappingShader("ShadowMapping.vs", "ShadowMapping.fs");

	static Shader shaderBlending("Blending.vs", "Blending.fs");
	shaderBlending.SetInt("texture1", 0);

	static Shader shadowMappingDepthShader("ShadowMappingDepth.vs", "ShadowMappingDepth.fs");

	static Shader skyboxShader("SkyboxShader.vs", "SkyboxShader.fs");

	static Shader objShaderTemp("ModelLoading.vs", "ModelLoading.fs");

	this->shadowMappingShader = shadowMappingShader;
	this->shaderBlending = shaderBlending;
	this->shadowMappingDepthShader = shadowMappingDepthShader;
	this->skyboxShader = skyboxShader;
	this->objShader = objShaderTemp;

	/* Shader Configurations */
	shadowMappingShader.Use();
	shadowMappingShader.SetInt("diffuseTexture", 0);
	shadowMappingShader.SetInt("shadowMap", 1);
	shadowMappingShader.SetFloat("brightness", 1.0);

	shadowMappingShader.SetInt("backgroundTexture", 2);
	shadowMappingShader.SetInt("rTexture", 0);
	shadowMappingShader.SetInt("gTexture", 0);
	shadowMappingShader.SetInt("bTexture", 3);
	shadowMappingShader.SetInt("blendMap", 5);

	objShader.Use();
	objShader.SetInt("diffuseTexture", 0);
	objShader.SetInt("shadowMap", 1);
	objShader.SetFloat("brightness", 1.0);

	objShader.SetInt("backgroundTexture", 2);
	objShader.SetInt("rTexture", 0);
	objShader.SetInt("gTexture", 0);
	objShader.SetInt("bTexture", 3);
	objShader.SetInt("blendMap", 5);
}

inline void Graphic::initializeObjects()
{
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-3.0f, 0.0f, 0.0f), glm::vec3(-3.0f, 0.0f, 0.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(10.0f, 0.0f, 10.0f), glm::vec3(10.0f, 0.0f, 10.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(7.0f, 0.0f, 16.0f), glm::vec3(7.0f, 0.0f, 16.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(20.0f, 0.0f, 9.0f), glm::vec3(20.0f, 0.0f, 9.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(5.0f, 0.0f, 13.0f), glm::vec3(5.0f, 0.0f, 13.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(18.0f, 0.0f, 25.0f), glm::vec3(18.0f, 0.0f, 25.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(16.0f, 0.0f, 2.0f), glm::vec3(16.0f, 0.0f, 2.0f)));

	objects.push_back(Entity(EntityID::TREE, glm::vec3(-14.0f, 0.0f, -10.0f), glm::vec3(-14.0f, 0.0f, -10.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-25.0f, 0.0f, 9.0f), glm::vec3(-25.0f, 0.0f, 9.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-8.0f, 0.0f, 25.0f), glm::vec3(-8.0f, 0.0f, 25.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-19.0f, 0.0f, -2.0f), glm::vec3(-19.0f, 0.0f, -2.0f)));

	objects.push_back(Entity(EntityID::TREE, glm::vec3(14.0f, 0.0f, -5.0f), glm::vec3(14.0f, 0.0f, -5.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(7.0f, 0.0f, -18.0f), glm::vec3(7.0f, 0.0f, -18.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(25.0f, 0.0f, -14.0f), glm::vec3(25.0f, 0.0f, -14.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(9.0f, 0.0f, -27.0f), glm::vec3(9.0f, 0.0f, -27.0f)));

	objects.push_back(Entity(EntityID::TREE, glm::vec3(-25.0f, 0.0f, 27.0f), glm::vec3(-25.0f, 0.0f, 27.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(25.0f, 0.0f, -27.0f), glm::vec3(25.0f, 0.0f, -27.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-25.0f, 0.0f, -27.0f), glm::vec3(-25.0f, 0.0f, -27.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(25.0f, 0.0f, 27.0f), glm::vec3(25.0f, 0.0f, 27.0f)));

	objects.push_back(Entity(EntityID::TREE, glm::vec3(-29.0f, 0.0f, 25.0f), glm::vec3(-29.0f, 0.0f, 25.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(29.0f, 0.0f, -25.0f), glm::vec3(29.0f, 0.0f, -25.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-29.0f, 0.0f, -25.0f), glm::vec3(-29.0f, 0.0f, -25.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(29.0f, 0.0f, 25.0f), glm::vec3(29.0f, 0.0f, 25.0f)));

	objects.push_back(Entity(EntityID::TREE, glm::vec3(-23.0f, 0.0f, 22.0f), glm::vec3(-23.0f, 0.0f, 22.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(23.0f, 0.0f, -22.0f), glm::vec3(23.0f, 0.0f, -22.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-23.0f, 0.0f, -22.0f), glm::vec3(-23.0f, 0.0f, -22.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(23.0f, 0.0f, 22.0f), glm::vec3(23.0f, 0.0f, 22.0f)));

	objects.push_back(Entity(EntityID::TREE, glm::vec3(0.3f, 0.0f, 8.8f), glm::vec3(0.3f, 0.0f, 8.8f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(2.7f, 0.0f, 10.4f), glm::vec3(2.7f, 0.0f, 10.4f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-5.4f, 0.0f, 13.7f), glm::vec3(-5.4f, 0.0f, 13.7f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-7.8f, 0.0f, 14.3f), glm::vec3(-7.8f, 0.0f, 14.3f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-9.0f, 0.0f, 13.0f), glm::vec3(-9.0f, 0.0f, 13.0f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-15.8f, 0.0f, -18.1f), glm::vec3(-15.8f, 0.0f, -18.1f)));
	objects.push_back(Entity(EntityID::TREE, glm::vec3(-11.4f, 0.0f, -18.2f), glm::vec3(-11.4f, 0.0f, -18.2f)));

	const float startX = -30.0f;
	const float startY = -30.0f;
	const float spaceX = 0.25f;
	const float spaceY = 0.25f;
	for (GLuint i = 0; i < 15; ++i) 
	{
		for (int j = 0; j < 15; ++j)
		{
			if (i == 0 || i == 14 || j == 0 || j == 14 )
			{
				objects.push_back(Entity(EntityID::TREE, glm::vec3(startX + i / spaceX, -0.25f, startY + j / spaceY), glm::vec3(startX + i / spaceX, -0.25f, startY + j / spaceY)));
			}
		}
	}

	objects.push_back(Entity(EntityID::WOOD_LOG, glm::vec3(-18.0f, 0.0f, 27.0f), glm::vec3(-18.0f, 0.0f, 27.0f)));
	objects.push_back(Entity(EntityID::CANNON, glm::vec3(-9.5f, 0.0f, 7.0f), glm::vec3(-9.5f, 0.0f, 7.0f)));
	objects.push_back(Entity(EntityID::CANNON_2, glm::vec3(-6.5f, 0.0f, 7.0f), glm::vec3(-6.5f, 0.0f, 7.0f)));
	objects.push_back(Entity(EntityID::CABIN, glm::vec3(-25.0f, 0.0f, -24.0f), glm::vec3(-25.0f, 0.0f, -24.0f)));
	objects.push_back(Entity(EntityID::CABIN_2, glm::vec3(12.0f, -0.5f, 28.0f), glm::vec3(12.0f, -0.5f, 28.0f)));
	objects.push_back(Entity(EntityID::FARM, glm::vec3(15.1f, -0.5f, -14.0f), glm::vec3(15.1f, -0.5f, -14.0f)));
	objects.push_back(Entity(EntityID::HOUSE, glm::vec3(-4.1f, -0.5f, -27.0f), glm::vec3(-4.1f, -0.5f, -27.0f)));
	
	objects.push_back(Entity(EntityID::WEL, glm::vec3(-12.5f, -0.5f, 18.0f), glm::vec3(-12.5f, -0.5f, 18.0f)));
	objects.push_back(Entity(EntityID::CASTLE, glm::vec3(-8.0f, 0.0f, -2.0f), glm::vec3(-8.0f, 0.0f, -2.0f)));

	SoundEngine->play2D("song.wav", GL_TRUE);
}

/* Configurations */

inline void Graphic::configureDepthMapFBO()
{
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	/* Attach depth texture as FBO's depth buffer */
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

inline void Graphic::configureObjects()
{
	/* Create camera */
	pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(1.0f, 1.0f, 1.0f));

	/* Set the Light Source Position */
	lightPos = glm::vec3(-30.0f, 15.0f, 0.0f);

	textureBrightness = 1.0f;
}


/* Creation Methods */
unsigned int Graphic::loadTexture(const std::string& strTexturePath)
{
	unsigned int textureId = -1;
	int width, height, nrChannels;

	/* Optional for flipping the loaded texture on the y-axis */
	//stbi_set_flip_vertically_on_load(true);

	/* Load image */
	unsigned char* data = stbi_load(strTexturePath.c_str(), &width, &height, &nrChannels, 0);

	if (data)
	{
		GLenum format;
		switch (nrChannels)
		{
		case 1:
			format = GL_RED;
			break;
		case 3:
			format = GL_RGB;
			break;
		case 4:
			format = GL_RGBA;
			break;
		default:
			format = GL_RGB;
			break;
		}

		/* Create texture and Generate mipmaps */
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		/* Set the texture wrapping parameters */
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);

		/* Set texture filtering parameters */
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else
	{
		std::cout << "Failed to load texture: " << strTexturePath << std::endl;
	}
	stbi_image_free(data);

	return textureId;
}

inline unsigned int Graphic::loadSkyboxTexture(std::vector<std::string> faces)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
			);
			stbi_image_free(data);
		}
		else
		{
			std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureID;
}

/* Render Methods */
void Graphic::renderPlayer(Shader& shader, glm::mat4& model)
{
	auto setShaderModel = [](Shader& shader, glm::mat4& model) { shader.SetMat4("model", model); };

	/* Render Player (Cube) */
	float rotate = pCamera->yaw / 57.3f;
	prepareShader(-1, shader, model);
	model = glm::translate(model, pCamera->player.position + glm::vec3(0.0f, -0.5f, 0.0f));

	model = glm::rotate(model, rotate, glm::vec3(0.05f, -1.0f, 0.0f));
	model = glm::rotate(model, 55.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::rotate(model, 55.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, moveRotate, glm::vec3(0.5f, 0.0f, 0.0f));


	if (choice == 1)
	{

		model = glm::scale(model, glm::vec3(0.0004f));
		setShaderModel(shader, model);
		this->models[9].DrawModel(objShader);
	}
	else
	{
		model = glm::scale(model, glm::vec3(0.004f));
		setShaderModel(shader, model);
		this->models[0].DrawModel(objShader);
	}
}


inline void Graphic::renderScene()
{
	glm::mat4 model = glm::mat4();
	auto setShaderModel = [](Shader& shader, glm::mat4& model) { shader.SetMat4("model", model); };

	Entity skybox(EntityID::SKYBOX);
	Entity floor(EntityID::FLOOR);
	Entity vegetation(EntityID::GRASS);

	/* Render the Ground */
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, textures[TextureID::GRASS]);
	shadowMappingShader.SetInt("diffuseTexture", 6);
	shadowMappingShader.SetBool("haveBlendMap", true);
	prepareShader(-1, shadowMappingShader, model);
	setShaderModel(shadowMappingShader, model);
	floor.render(shadowMappingShader, model);

	/* Render the Player */
	shadowMappingShader.SetInt("diffuseTexture", 0);
	shadowMappingShader.SetBool("haveBlendMap", false);
	renderPlayer(shadowMappingShader, model);



	/* Render the Objects */
	for (int i = 0; i < objects.size(); ++i)
	{
		if (objects[i].getID() == EntityID::TREE)
		{
			prepareShader(-1, shadowMappingShader, model);
			model = glm::translate(model, objects[i].getRenderPos() - glm::vec3(0.0f, 0.5f, 0.0f)); // glm::vec3(3.0f, 0.0f, -0.7f));
			model = glm::rotate(model, 180.0f, glm::vec3(1.0f, 1.0f, 1.0f));
			setShaderModel(shadowMappingShader, model);
			this->models[this->models.size() - 1].DrawModel(objShader);
			model = glm::mat4();
		}
	}

	//woods
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-18.0f, 0.0f, 27.0f));
	model = glm::scale(model, glm::vec3(0.04f, 0.04f, 0.04f));
	setShaderModel(shadowMappingShader, model);
	models[2].DrawModel(objShader);

	//house
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-34.0f, -0.5f, -3.0f));
	model = glm::scale(model, glm::vec3(0.6f, 0.6f, 0.6f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setShaderModel(shadowMappingShader, model);
	models[4].DrawModel(objShader);


	//cabin
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-25.0f, -0.5f, -24.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::rotate(model, glm::radians(-45.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setShaderModel(shadowMappingShader, model);
	models[6].DrawModel(objShader);

	////cannon
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-9.5f, -0.5f, 7.0f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 0.f, 1.f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::scale(model, glm::vec3(0.01f));
	setShaderModel(shadowMappingShader, model);
	models[3].DrawModel(objShader);


	////cannon2
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-6.5f, -0.5f, 7.0f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 0.f, 1.f));

	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::scale(model, glm::vec3(0.01f));
	setShaderModel(shadowMappingShader, model);
	models[3].DrawModel(objShader);



	////rocks
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-2.3f, -0.5f, -19.0f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 0.f, 1.f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::scale(model, glm::vec3(0.1f));
	setShaderModel(shadowMappingShader, model);
	models[7].DrawModel(objShader);

	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-2.1f, -0.5f, -19.0f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 0.f, 1.f));
	model = glm::rotate(model, glm::radians(290.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::scale(model, glm::vec3(0.1f));
	setShaderModel(shadowMappingShader, model);
	models[7].DrawModel(objShader);

	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-2.1f, -0.5f, -18.0f));
	model = glm::rotate(model, glm::radians(100.f), glm::vec3(0.f, 0.f, 1.f));
	model = glm::rotate(model, glm::radians(290.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::scale(model, glm::vec3(0.1f));
	setShaderModel(shadowMappingShader, model);
	models[7].DrawModel(objShader);


	////other house
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-4.1f, -0.5f, -27.0f));
	model = glm::scale(model, glm::vec3(0.6f, 0.6f, 0.6f));
	model = glm::rotate(model, glm::radians(360.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setShaderModel(shadowMappingShader, model);
	models[4].DrawModel(objShader);

	////farm
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(15.1f, -0.5f, -14.0f));
	model = glm::scale(model, glm::vec3(0.15f, 0.15f, 0.15f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setShaderModel(shadowMappingShader, model);
	models[8].DrawModel(objShader);


	////other cabin
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(12.0f, -0.5f, 28.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setShaderModel(shadowMappingShader, model);
	models[6].DrawModel(objShader);

	////horse
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(15.0f, -0.5f, -11.0f));
	model = glm::scale(model, glm::vec3(0.0005f, 0.0005f, 0.0005f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	setShaderModel(shadowMappingShader, model);
	models[9].DrawModel(objShader);


	////well
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-12.5f, -0.5f, 18.0f));
	model = glm::scale(model, glm::vec3(0.08f));
	setShaderModel(shadowMappingShader, model);
	models[10].DrawModel(objShader);

	////castle
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-8.0f, -0.7f, -2.0f));
	model = glm::scale(model, glm::vec3(0.35f));
	setShaderModel(shadowMappingShader, model);
	models[11].DrawModel(objShader);

	////ducks
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-11.0f, -0.5f, 19.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	setShaderModel(shadowMappingShader, model);
	models[5].DrawModel(objShader);

	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-13.0f, -0.5f, 18.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(-50.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	setShaderModel(shadowMappingShader, model);
	models[5].DrawModel(objShader);

	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-14.0f, -0.5f, 17.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(150.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	setShaderModel(shadowMappingShader, model);
	models[5].DrawModel(objShader);

	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(-13.0f, -0.5f, 20.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(-150.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	setShaderModel(shadowMappingShader, model);
	models[5].DrawModel(objShader);


	////cats
	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(8.0f, -0.5f, 27.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(-180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	setShaderModel(shadowMappingShader, model);
	models[12].DrawModel(objShader);

	prepareShader(-1, shadowMappingShader, model);
	model = glm::translate(model, glm::vec3(20.0f, -0.5f, 25.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(-180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	setShaderModel(shadowMappingShader, model);
	models[12].DrawModel(objShader);

	/* Render the Vegetation */
	model = glm::mat4();
	prepareShader(textures[TextureID::GRASS_BUSH], shaderBlending, model);
	vegetation.setFirst(first);
	vegetation.render(shaderBlending, model);

	/* Render the Skybox */
	prepareSkyboxShader(textures[TextureID::SKYBOX], skyboxShader, model);
	skybox.render(skyboxShader, model);

	if (first)
	{
		first = false;
	}
	if (sheepSound == 0)
	{
		if (choice == 0)
		{
			SoundEngine->play2D("sheep.wav", GL_FALSE);
			sheepSound = 150;
		}
		else
		{
			SoundEngine->play2D("horse.wav", GL_FALSE);
			sheepSound = 150;
		}
	}
	else
	{
		sheepSound--;
	}
}

inline void Graphic::renderScene(Shader& shader)
{
	glm::mat4 model;
	auto setShaderModel = [](Shader& shader, glm::mat4& model) { shader.SetMat4("model", model); };

	/* Render the Player */
	renderPlayer(shader, model);

	/* Render the Objects */
	for (int i = 0; i < objects.size(); ++i)
	{
		if (objects[i].getID() == EntityID::TREE)
		{
			prepareShader(-1, shader, model);
			model = glm::translate(model, objects[i].getRenderPos() - glm::vec3(0.0f, 0.5f, 0.0f)); // glm::vec3(3.0f, 0.0f, -0.7f));
			model = glm::rotate(model, 180.0f, glm::vec3(1.0f, 1.0f, 1.0f));
			setShaderModel(shader, model);
			this->models[this->models.size() - 1].DrawModel(objShader);
			model = glm::mat4();
		}
	}

	//woods
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-18.0f, 0.0f, 27.0f));
	model = glm::scale(model, glm::vec3(0.04f, 0.04f, 0.04f));
	setShaderModel(shader, model);
	models[2].DrawModel(objShader);

	//house
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-34.0f, -0.5f, -3.0f));
	model = glm::scale(model, glm::vec3(0.6f, 0.6f, 0.6f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setShaderModel(shader, model);
	models[4].DrawModel(objShader);


	//cabin
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-25.0f, -0.5f, -24.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::rotate(model, glm::radians(-45.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setShaderModel(shader, model);
	models[6].DrawModel(objShader);

	////cannon
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-9.5f, -0.5f, 7.0f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 0.f, 1.f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::scale(model, glm::vec3(0.01f));
	setShaderModel(shader, model);
	models[3].DrawModel(objShader);


	////cannon2
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-6.5f, -0.5f, 7.0f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 0.f, 1.f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::scale(model, glm::vec3(0.01f));
	setShaderModel(shader, model);
	models[3].DrawModel(objShader);



	////rocks
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-2.3f, -0.5f, -19.0f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 0.f, 1.f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::scale(model, glm::vec3(0.1f));
	setShaderModel(shader, model);
	models[7].DrawModel(objShader);

	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-2.1f, -0.5f, -19.0f));
	model = glm::rotate(model, glm::radians(270.f), glm::vec3(0.f, 0.f, 1.f));
	model = glm::rotate(model, glm::radians(290.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::scale(model, glm::vec3(0.1f));
	setShaderModel(shader, model);
	models[7].DrawModel(objShader);

	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-2.1f, -0.5f, -18.0f));
	model = glm::rotate(model, glm::radians(100.f), glm::vec3(0.f, 0.f, 1.f));
	model = glm::rotate(model, glm::radians(290.f), glm::vec3(0.f, 1.f, 0.f));
	model = glm::scale(model, glm::vec3(0.1f));
	setShaderModel(shader, model);
	models[7].DrawModel(objShader);


	////other house
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-4.1f, -0.5f, -27.0f));
	model = glm::scale(model, glm::vec3(0.6f, 0.6f, 0.6f));
	model = glm::rotate(model, glm::radians(360.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setShaderModel(shader, model);
	models[4].DrawModel(objShader);

	////farm
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(15.1f, -0.5f, -14.0f));
	model = glm::scale(model, glm::vec3(0.15f, 0.15f, 0.15f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setShaderModel(shader, model);
	models[8].DrawModel(objShader);


	////other cabin
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(12.0f, -0.5f, 28.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setShaderModel(shader, model);
	models[6].DrawModel(objShader);

	////horse
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(15.0f, -0.5f, -11.0f));
	model = glm::scale(model, glm::vec3(0.0005f, 0.0005f, 0.0005f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	setShaderModel(shader, model);
	models[9].DrawModel(objShader);


	////well
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-12.5f, -0.5f, 18.0f));
	model = glm::scale(model, glm::vec3(0.08f));
	setShaderModel(shader, model);
	models[10].DrawModel(objShader);

	////castle
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-8.0f, -0.7f, -2.0f));
	model = glm::scale(model, glm::vec3(0.35f));
	setShaderModel(shader, model);
	models[11].DrawModel(objShader);

	////ducks
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-11.0f, -0.5f, 19.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	setShaderModel(shader, model);
	models[5].DrawModel(objShader);

	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-13.0f, -0.5f, 18.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(-50.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	setShaderModel(shader, model);
	models[5].DrawModel(objShader);

	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-14.0f, -0.5f, 17.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(150.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	setShaderModel(shader, model);
	models[5].DrawModel(objShader);

	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(-13.0f, -0.5f, 20.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(-150.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	setShaderModel(shader, model);
	models[5].DrawModel(objShader);


	////cats
	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(8.0f, -0.5f, 27.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(-180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	setShaderModel(shader, model);
	models[12].DrawModel(objShader);

	prepareShader(-1, shader, model);
	model = glm::translate(model, glm::vec3(20.0f, -0.5f, 25.0f));
	model = glm::scale(model, glm::vec3(0.01f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(-180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	setShaderModel(shader, model);
	models[12].DrawModel(objShader);

	/* Render the Vegetation */
	//Entity vegetation(EntityID::GRASS);
	//model = glm::mat4();
	//prepareShader(textures[TextureID::GRASS_BUSH], shader, model);
	//vegetation.setFirst(first);
	//vegetation.render(shader, model);
}

inline void Graphic::renderDepthOfSceneToTexture()
{
	/* 1. Render depth of scene to texture (from light's perspective) */
	glm::mat4 lightProjection, lightView;
	float near_plane = 1.0f, far_plane = 60.0f;
	lightProjection = glm::ortho(-60.0f, 60.0f, -60.0f, 60.0f, near_plane, far_plane);
	lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, -30.0, 0.0));
	lightSpaceMatrix = lightProjection * lightView;

	/* Render scene from light's point of view */
	shadowMappingDepthShader.Use();
	shadowMappingDepthShader.SetMat4("lightSpaceMatrix", lightSpaceMatrix);

	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[TextureID::DIRT]);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, textures[TextureID::GRASS]);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, textures[TextureID::GRAVEL]);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, textures[TextureID::GRASS_BUSH]);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, textures[TextureID::BLENDMAP]);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, textures[TextureID::BLENDMAP_DIFFUSE]);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	renderScene(shadowMappingDepthShader);
	glCullFace(GL_BACK);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

inline void Graphic::renderGeneratedDepthShadowMap()
{
	/* 2. Render scene as normal using the generated depth/shadow map */
	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	shadowMappingShader.Use();
	glm::mat4 projection = pCamera->GetProjectionMatrix();
	glm::mat4 view = pCamera->GetViewMatrix();
	shadowMappingShader.SetMat4("projection", projection);
	shadowMappingShader.SetMat4("view", view);

	/* Set light uniforms */
	shadowMappingShader.SetVec3("viewPos", pCamera->GetPosition());
	shadowMappingShader.SetVec3("lightPos", lightPos);
	shadowMappingShader.SetMat4("lightSpaceMatrix", lightSpaceMatrix);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[TextureID::DIRT]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glDisable(GL_CULL_FACE);
}

inline void Graphic::renderShadows()
{
	/* Reset Viewport */
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderDepthOfSceneToTexture();

	/* Reset Viewport */
	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderGeneratedDepthShadowMap();
}

void Graphic::renderCube()
{
	unsigned int VAO = 0, VBO = 0;

	if (VAO == 0)
	{
		const float size = 1.0f;
		float vertices[] = {
			// back face
			-size, -size, -size,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			 size,  size, -size,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			 size, -size, -size,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
			 size,  size, -size,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			-size, -size, -size,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			-size,  size, -size,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
			// front face
			-size, -size,  size,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
			 size, -size,  size,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
			 size,  size,  size,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
			 size,  size,  size,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
			-size,  size,  size,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
			-size, -size,  size,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
			// left face
			-size,  size,  size, -size,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
			-size,  size, -size, -size,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
			-size, -size, -size, -size,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
			-size, -size, -size, -size,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
			-size, -size,  size, -size,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
			-size,  size,  size, -size,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
			// right face
			size,  size,  size,  size,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
			size, -size, -size,  size,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
			size,  size, -size,  size,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
			size, -size, -size,  size,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
			size,  size,  size,  size,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
			size, -size,  size,  size,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
			// bottom face
			-size, -size, -size,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
			 size, -size, -size,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
			 size, -size,  size,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
			 size, -size,  size,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
			-size, -size,  size,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
			-size, -size, -size,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
			// top face
			-size,  size, -size,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
			 size,  size , size,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
			 size,  size, -size,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
			 size,  size,  size,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
			-size,  size, -size,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
			-size,  size,  size,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left			    
		};
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);

		/* Fill buffer */
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		/* Link vertex attributes */
		glBindVertexArray(VAO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	/* Render Cube */
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

void Graphic::renderFloor()
{
	unsigned int planeVAO = 0, planeVBO = 0;

	if (planeVAO == 0) {
		/* Set up vertex data (and buffer(s)) and configure vertex attributes */
		float planeVertices[] = {
			// positions            // normals         // texcoords
			25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
			-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

			25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
			25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f
		};
		/* Plane VAO */
		glGenVertexArrays(1, &planeVAO);
		glGenBuffers(1, &planeVBO);
		glBindVertexArray(planeVAO);
		glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindVertexArray(0);
	}

	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

void Graphic::renderGrass(const Shader& shaderBlending, glm::mat4&model)
{
	unsigned int grassVBO, grassVAO;

	/* Grass vertices */
	float grassVertices[] = {
		// positions                   // texcoords
		-0.5f,  0.5f,  0.0f,    0.0f,  0.0f,
		-0.5f, -0.5f,  0.0f,     0.0f,  1.0f,
		 0.5f, -0.5f,  0.0f,    1.0f, 1.0f,

		-0.5f,  0.5f,  0.0f,    0.0f,  0.0f,
		 0.5f, -0.5f,  0.0f,     1.0f,  1.0f,
		 0.5f,  0.5f,  0.0f,    1.0f, 0.0f
	};
	glGenVertexArrays(1, &grassVAO);
	glGenBuffers(1, &grassVBO);
	glBindVertexArray(grassVAO);
	glBindBuffer(GL_ARRAY_BUFFER, grassVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(grassVertices), grassVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

	for (int j = 0; j <= 10; j++) {
		model = glm::rotate(model, glm::radians(j*18.f), glm::vec3(0.f, 1.f, 0.f));
		shaderBlending.SetMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

}

inline void Graphic::renderBunchOfGrass(const Shader& shaderBlending, glm::mat4& model)
{
	unsigned int bunchVAO = 0;

	/* Draw vegetation */
	glBindVertexArray(bunchVAO);
	glBindTexture(GL_TEXTURE_2D, textures[TextureID::GRASS_BUSH]);
	if (1) {
		glBindVertexArray(bunchVAO);
		glBindTexture(GL_TEXTURE_2D, textures[TextureID::GRASS_BUSH]);
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(0.f, 0.f, 0.f));
		renderGrass(shaderBlending, model);
		for (GLuint i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++)
			{
				model = glm::mat4();
				glm::vec3 bunchPosition(-5.f + i / 2.f, 0, -5.f + j / 2.f);
				model = glm::translate(model, bunchPosition);
				renderGrass(shaderBlending, model);
			}
		}

	}
}



/* Helper Methods */
void Graphic::setWindowedFullscreen()
{
	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	SCR_WIDTH = mode->width;
	SCR_HEIGHT = mode->height;
}

void  Graphic::processInput(GLFWwindow *window)
{
	/* Process all input: query GLFW whether relevant keys are
		pressed/released this frame and react accordingly */

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	Camera::MovementType movement = Camera::MovementType::UNKNOWN;

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		movement = Camera::MovementType::FORWARD;
		moveRotate = moveRotate + 0.05f;
		
		if (moveRotate >= 0.5f)
		{
			moveRotate = -moveRotate;
		}
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		movement = Camera::MovementType::BACKWARD;
	
		moveRotate = moveRotate - 0.05f;
		if (moveRotate <= -0.5f)
		{
			moveRotate = 0.5f;
		}
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		movement = Camera::MovementType::LEFT;
	
		
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		movement = Camera::MovementType::RIGHT;
	
	}
	if (movement != Camera::MovementType::UNKNOWN)
	{
		glm::vec3 nextPlayerPosition = pCamera->getPlayerNextPosition(movement, (float)deltaTime);
		bool canMove = true;

		/* Check for Collision */
		for (Entity object : objects)
		{
			glm::vec3 size = glm::vec3(0.2f, 0.2f, 0.2f);
			glm::vec3 objectOffset = glm::vec3(0.0f, 0.0f, 0.0f);

			switch (object.getID())
			{
			case EntityID::TREE:
				size = glm::vec3(0.5f, 0.5f, 0.5f);
				break;
			case EntityID::WOOD_LOG:
				size = glm::vec3(1.8f, 0.5f, 3.0f);
				objectOffset = glm::vec3(-0.4f, 0.0f, -1.35f);
				break;
			case EntityID::CANNON:
				size = glm::vec3(0.6f, 0.5f, 0.7f);
				objectOffset = glm::vec3(-0.2f, 0.0f, -0.2f);
				break;
			case EntityID::CANNON_2:
				size = glm::vec3(0.6f, 0.5f, 0.7f);
				objectOffset = glm::vec3(-0.2f, 0.0f, -0.2f);
				break;
			case EntityID::CABIN:
				size = glm::vec3(3.5f, 0.5f, 3.5f);
				objectOffset = glm::vec3(-1.5f, 0.0f, -1.5f);
				break;
			case EntityID::FARM:
				size = glm::vec3(7.0f, 0.5f, 3.0f);
				objectOffset = glm::vec3(-4.0f, 0.0f, -1.4f);
				break;
			case EntityID::HOUSE:
				size = glm::vec3(8.1f, 0.5f, 4.0f);
				objectOffset = glm::vec3(-4.0f, 0.0f, -1.9f);
				break;
			case EntityID::CABIN_2:
				size = glm::vec3(3.5f, 0.5f, 3.5f);
				objectOffset = glm::vec3(-1.6f, 0.0f, -1.2f);
				break;
			case EntityID::WEL:
				size = glm::vec3(0.9f, 0.5f, 1.2f);
				objectOffset = glm::vec3(-0.3f, 0.0f, -0.5f);
				break;
			case EntityID::CASTLE:
				size = glm::vec3(18.0f, 0.5f, 21.20f);
				objectOffset = glm::vec3(-9.0f, 0.0f, -13.0f);
				break;

			default:
				break;
			}

			if (collision(nextPlayerPosition, glm::vec3(0.2f, 0.2f, 0.2f),
				object.getWorldPos() + objectOffset, size))
			{

				canMove = false;
			}
		}

		/* Collision with the outer invisible walls */
		glm::vec3 northWest = glm::vec3(-30.0f, 0.2f, 30.0f);
		glm::vec3 northEast = glm::vec3(-30.0f, 0.2f, -30.0f);
		glm::vec3 southEast = glm::vec3(30.0f, 0.2f, -30.0f);
		if (collision(nextPlayerPosition, glm::vec3(0.2f, 0.2f, 0.2f),
			northWest, glm::vec3(60.0f, 0.2f, 0.0f)) ||
			collision(nextPlayerPosition, glm::vec3(0.2f, 0.2f, 0.2f),
				northEast, glm::vec3(60.0f, 0.2f, 0.0f)) ||
			collision(nextPlayerPosition, glm::vec3(0.2f, 0.2f, 0.2f),
				northEast, glm::vec3(0.0f, 0.2f, 60.0f)) ||
			collision(nextPlayerPosition, glm::vec3(0.2f, 0.2f, 0.2f),
				southEast, glm::vec3(0.0f, 0.2f, 60.0f)))
		{
			
			canMove = false;
			
		}

		if (canMove)
		{
			pCamera->ProcessKeyboard(movement, (float)deltaTime);
		}
		else
		{
			SoundEngine->play2D("hit.wav", GL_FALSE);
		}
	}

	if (glfwGetKey(window, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS)
	{
		//if (textureBrightness > 0.2f)
		textureBrightness -= 0.1f;
	}
	else if (glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS)
	{
		//if ( textureBrightness < 2.0f)
		textureBrightness += 0.1f;
	}
	shadowMappingShader.SetFloat("brightness", textureBrightness);

	std::string skyboxVersion = "";
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
	{
		skyboxVersion = "v2";
	}
	else if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
	{
		skyboxVersion = "v2";
	}
	else if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
	{
		skyboxVersion = "v3.1";
	}
	else if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
	{
		skyboxVersion = "v3.2";
	}
	else if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS)
	{
		skyboxVersion = "v3.3";
	}
	else if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS)
	{
		skyboxVersion = "v3.4";
	}
	if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS)
	{
		choice = 1;
	}
	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS)
	{
		choice = 0;
	}
	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS)
	{
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	}
	

	if (skyboxVersion != "")
	{
		std::vector<std::string> skyboxFaces;
		skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + skyboxVersion + "\\right.jpg");
		skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + skyboxVersion + "\\left.jpg");
		skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + skyboxVersion + "\\top.jpg");
		skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + skyboxVersion + "\\bottom.jpg");
		skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + skyboxVersion + "\\front.jpg");
		skyboxFaces.push_back(strExePath + "\\Resources\\Skybox\\" + skyboxVersion + "\\back.jpg");
		textures[TextureID::SKYBOX] = loadSkyboxTexture(skyboxFaces);
	}

	if (glfwGetKey(window, GLFW_KEY_PAGE_UP) == GLFW_PRESS)
		pCamera->ProcessKeyboard(Camera::MovementType::UP, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS)
		pCamera->ProcessKeyboard(Camera::MovementType::DOWN, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		pCamera->Reset(width, height);
	}
}

inline void Graphic::cleanup()
{
	delete pCamera;
}

inline void Graphic::setCurrentShader(Shader & shader)
{
	shader.Use();
	glm::mat4 projection = pCamera->GetProjectionMatrix();
	glm::mat4 view = pCamera->GetViewMatrix();

	shader.SetMat4("projection", projection);
	shader.SetMat4("view", view);
}


inline void Graphic::setCurrentSkyboxShader(Shader & shader)
{
	shader.Use();
	glm::mat4 projection = pCamera->GetProjectionMatrix();
	glm::mat4 view = glm::mat4(glm::mat3(pCamera->GetViewMatrix()));

	shader.SetMat4("projection", projection);
	shader.SetMat4("view", view);
}

inline void Graphic::setCurrentTexture(const unsigned int & textureID)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glDisable(GL_CULL_FACE);
}


inline void Graphic::prepareShader(const unsigned int textureID, Shader& shader, glm::mat4 & model)
{
	auto resetModel = [](glm::mat4& model) { model = glm::mat4(); };

	setCurrentShader(shader);
	setCurrentTexture(textureID);
	resetModel(model);
}

inline void Graphic::prepareSkyboxShader(const unsigned int textureID, Shader & shader, glm::mat4 & model)
{
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_FALSE);

	shader.Use();
	glm::mat4 projection = pCamera->GetProjectionMatrix();
	glm::mat4 view = glm::mat4(glm::mat3(pCamera->GetViewMatrix()));

	shader.SetMat4("projection", projection);
	shader.SetMat4("view", view);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	model = glm::mat4();
}

inline void Graphic::frameTimeLogic()
{
	/* Per-frame time logic */
	double currentFrame = glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;
	
}

bool Graphic::collision(glm::vec3 a, glm::vec3 aSize, glm::vec3 b, glm::vec3 bSize)
{
	return (a.x <= (b.x + bSize.x) && (a.x + aSize.x) >= b.x) &&
		(a.y <= (b.y + bSize.y) && (a.y + aSize.y) >= b.y) &&
		(a.z <= (b.z + bSize.z) && (a.z + aSize.z) >= b.z);
}


/* GLFW Callbacks: whenever the window size changed (by OS or user resize) this callback functions executes */
inline void Graphic::framebuffer_size_callback(GLFWwindow * window, int width, int height)
{
	/* Make sure the viewport matches the new window dimensions; Note that width and
	height will be significantly larger than specified on retina displays. */
	pCamera->Reshape(width, height);
}

inline void Graphic::mouse_callback(GLFWwindow * window, double xpos, double ypos)
{
	pCamera->MouseControl((float)xpos, (float)ypos);
}

inline void Graphic::scroll_callback(GLFWwindow * window, double xoffset, double yOffset)
{
	pCamera->ProcessMouseScroll((float)yOffset);
}
