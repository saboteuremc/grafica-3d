#pragma once

#include "pch.h"
#include <stdlib.h> // Needed for shader reading
#include <stdio.h>

#include <GL/glew.h>

#define GLM_FORCE_CTOR_INIT 
#include <GLM.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <glfw3.h>

#include <iostream>
#include <fstream>
#include <sstream>


//#include <assimp/Importer.hpp>
//#include <assimp/scene.h>
//#include <assimp/postprocess.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#pragma comment (lib, "glfw3dll.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")
//#pragma comment(lib, "assimp-vc140-mt.lib")

#include <math.h> 
#include <vector>
#include <map>

enum class EntityID
{
	UNKNOWN,
	CUBE,
	GRASS,
	FLOOR,
	SKYBOX,
	TREE,
	WOOD_LOG,
	CANNON,
	CANNON_2,
	CABIN,
	CABIN_2,
	FARM,
	HOUSE,
	WEL,
	CASTLE
};