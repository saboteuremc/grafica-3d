#pragma once

#include "Includes.h"
#include <ctime>
#include <random>

std::vector<std::vector<int>> newVegetation({
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1 },
	{ 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
	{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1 },
	{ 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1 },
	{ 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1 },
	{ 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1 },
	{ 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1 },
	{ 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1 },
	{ 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1 },
	{ 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1 },
	{ 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1 },
	{ 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1 },
	{ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
	});

const float cubeVertices[] = {
	// back face
	-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
	1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
	1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
	1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
	-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
	-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
	// front face
	-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
	1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
	1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
	1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
	-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
	-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
	// left face
	-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
	-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
	-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
	-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
	-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
	-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
	// right face
	1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
	1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
	1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
	1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
	1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
	1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
	// bottom face
	-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
	1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
	1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
	1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
	-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
	-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
	// top face
	-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
	1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
	1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
	1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
	-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
	-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left			    
};

const float planeVertices[] = {
	// positions            // normals         // texcoords
	25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
	-25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
	-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
	25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
	-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
	25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f
};

const float cubeFloorVertices[] = {
	// back face
	-0.0f, -0.0f, -0.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
	0.0f,  0.0f, -0.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
	0.0f, -0.0f, -0.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
	0.0f,  0.0f, -0.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
	-0.0f, -0.0f, -0.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
	-0.0f,  0.0f, -0.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
	// front face
	-0.0f, -0.0f,  0.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
	0.0f, -0.0f,  0.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
	0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
	0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
	-0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
	-0.0f, -0.0f,  0.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
	// left face
	-0.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
	-0.0f,  0.0f, -0.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
	-0.0f, -0.0f, -0.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
	-0.0f, -0.0f, -0.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
	-0.0f, -0.0f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
	-0.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
	// right face
	0.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
	0.0f, -0.0f, -0.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
	0.0f,  0.0f, -0.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
	0.0f, -0.0f, -0.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
	0.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
	0.0f, -0.0f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
	// bottom face
	-0.0f, -0.0f, -0.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
	0.0f, -0.0f, -0.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
	0.0f, -0.0f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
	0.0f, -0.0f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
	-0.0f, -0.0f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
	-0.0f, -0.0f, -0.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
	// top face
	-60.0f,  -0.5f, -60.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
	60.0f,  -0.5f , 60.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
	60.0f,  -0.5f, -60.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
	60.0f,  -0.5f,  60.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
	-60.0f,  -0.5f, -60.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
	-60.0f,  -0.5f,  60.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left			    
	};

const float floorVertices[] = {
	// positions                   // texcoords
	25.0f, -0.5f,  25.0f,    1.0f,  0.0f,
	-25.0f, -0.5f,  25.0f,     0.0f,  0.0f,
	-25.0f, -0.5f, -25.0f,    0.0f, 1.0f,

	25.0f, -0.5f,  25.0f,   1.0f,  0.0f,
	-25.0f, -0.5f, -25.0f,   0.0f, 1.0f,
	25.0f, -0.5f, -25.0f,    1.0f, 1.0f
};

/* Grass vertices */
const float grassVertices[] = {
	// positions                   // texcoords
	-0.5f,  0.5f,  0.0f,    0.0f,  0.0f,
	-0.5f, -0.5f,  0.0f,     0.0f,  1.0f,
	0.5f, -0.5f,  0.0f,    1.0f, 1.0f,

	-0.5f,  0.5f,  0.0f,    0.0f,  0.0f,
	0.5f, -0.5f,  0.0f,     1.0f,  1.0f,
	0.5f,  0.5f,  0.0f,    1.0f, 0.0f
};

const float skyboxVertices[] = {
	// positions          
	-1.0f,  1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	-1.0f,  1.0f, -1.0f,
	1.0f,  1.0f, -1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	1.0f, -1.0f,  1.0f
};

enum class TextureID
{
	DIRT,
	GRASS_BUSH,
	BRICK,
	GRASS,
	MUD,
	GRAVEL,
	BLENDMAP,
	BLENDMAP_DIFFUSE,
	PINKFLOWERS,
	SKYBOX
};

class Entity
{
public:
	Entity(const EntityID& type = EntityID::UNKNOWN, 
			const glm::vec3& renderPos = glm::vec3(0.0f, 0.0f, 0.0f), 
			const glm::vec3& worldPos = glm::vec3(0.0f, 0.0f, 0.0f));
	~Entity() = default;

	/* Render the Object with based on the specified Type */
	void render(const Shader& shader, glm::mat4& model);

	/* Getters */
	glm::vec3 getRenderPos();
	glm::vec3 getWorldPos();
	EntityID getID();

	/* Setters */
	void setRenderPos(const glm::vec3& position);
	void setWorldPos(const glm::vec3& position);
	void setFirst(const bool& first);

private:
	void renderCube();
	void renderFloorSeamless();
	void renderCubeFloor();
	void renderFloor();
	void renderGrass(const Shader& shader, glm::mat4& model);
	void renderSkybox();
	void renderTheVegetation(const Shader& shaderBlending, glm::mat4& model);

	void setVegetation();

public:
	/* Position that is used to render in the Scene */
	glm::vec3 renderPos;

	/* Position that is used for Collision Detection */
	glm::vec3 worldPos;

	/* The Type that is used to reconise the corresponding object */
	EntityID type;

	/* Vegetation */
	std::vector<std::vector<int>> vegetation;

	bool first;
};





























//=================================================================================//
//								   FUNCTIONS BODY								   //
//=================================================================================//

inline Entity::Entity(const EntityID& type, const glm::vec3 & renderPos, const glm::vec3 & worldPos)
{
	this->renderPos = renderPos;
	this->worldPos = worldPos;
	this->type = type;
	this->first = true;
}

inline void Entity::render(const Shader& shader, glm::mat4& model)
{
	switch (type)
	{
	case EntityID::FLOOR:
		renderCubeFloor();
		break;
	case EntityID::GRASS:
		setVegetation();
		renderTheVegetation(shader, model);
		break;
	case EntityID::CUBE:
		renderCube();
		break;
	case EntityID::SKYBOX:
		renderSkybox();
		break;
	case EntityID::TREE:
		renderCube();
		break;

	default:
		std::cout << "ERROR: No corresponding type " << (char)type << " found!" << std::endl;
		break;
	}
}

inline glm::vec3 Entity::getRenderPos()
{
	return this->renderPos;
}

inline glm::vec3 Entity::getWorldPos()
{
	return this->worldPos;
}

inline EntityID Entity::getID()
{
	return this->type;
}

inline void Entity::setRenderPos(const glm::vec3 & position)
{
	this->renderPos = position;
}

inline void Entity::setWorldPos(const glm::vec3 & position)
{
	this->worldPos = position;
}

inline void Entity::setFirst(const bool & first)
{
	this->first = first;
}

void Entity::renderCube()
{
	unsigned int VAO = 0, VBO = 0;

	if (VAO == 0)
	{		
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);

		/* Fill buffer */
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

		/* Link vertex attributes */
		glBindVertexArray(VAO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	/* Render Cube */
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

void Entity::renderFloorSeamless()
{
	unsigned int planeVAO = 0, planeVBO = 0;

	if (planeVAO == 0) {
		/* Set up vertex data (and buffer(s)) and configure vertex attributes */

		/* Plane VAO */
		glGenVertexArrays(1, &planeVAO);
		glGenBuffers(1, &planeVBO);
		glBindVertexArray(planeVAO);
		glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindVertexArray(0);
	}

	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

inline void Entity::renderCubeFloor()
{
	unsigned int VAO = 0, VBO = 0;

	if (VAO == 0)
	{
		
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);

		/* Fill buffer */
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeFloorVertices), cubeFloorVertices, GL_STATIC_DRAW);

		/* Link vertex attributes */
		glBindVertexArray(VAO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	/* Render Cube */
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

inline void Entity::renderFloor()
{

	unsigned int floorVBO, floorVAO;
	// Floor VAO si VBO
	glGenVertexArrays(1, &floorVAO);
	glGenBuffers(1, &floorVBO);
	glBindVertexArray(floorVAO);
	glBindBuffer(GL_ARRAY_BUFFER, floorVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(floorVertices), floorVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

	glBindVertexArray(floorVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

void Entity::renderGrass(const Shader& shaderBlending, glm::mat4&model)
{
	unsigned int grassVBO, grassVAO;

	glGenVertexArrays(1, &grassVAO);
	glGenBuffers(1, &grassVBO);
	glBindVertexArray(grassVAO);
	glBindBuffer(GL_ARRAY_BUFFER, grassVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(grassVertices), grassVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

	for (int j = 0; j <= 1; j++) {
		model = glm::rotate(model, glm::radians(j*90.f), glm::vec3(0.f, 1.f, 0.f));
		shaderBlending.SetMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}
}

inline void Entity::renderSkybox()
{
	unsigned int skyboxVBO = 0, skyboxVAO = 0;

	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1, &skyboxVBO);

	/* Fill buffer */
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);

	/* Link vertex attributes */
	glBindVertexArray(skyboxVAO);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)(6 * sizeof(float)));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glBindVertexArray(skyboxVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);

	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LESS);
}

inline void Entity::renderTheVegetation(const Shader& shader, glm::mat4& model)
{
	unsigned int bunchVAO = 0;

	/* Draw vegetation */
	glBindVertexArray(bunchVAO);
	//glBindTexture(GL_TEXTURE_2D, textures[TextureID::GRASS_BUSH]);
	if (1) {
		glBindVertexArray(bunchVAO);
		//glBindTexture(GL_TEXTURE_2D, textures[TextureID::GRASS_BUSH]);
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(5.f, 0.f, 5.f));

		const float startX = -30.0f;
		const float startY = -30.0f;

		const float spaceX = 0.5f;
		const float spaceY = 0.5f;

		for (GLuint i = 0; i < 30; i++) {
			for (int j = 0; j < 30; j++)
			{
				if (vegetation[i][j] == 2)
				{
					model = glm::mat4();
					glm::vec3 bunchPosition(startX + i / spaceX, -0.25f, startY + j / spaceY);
					model = glm::translate(model, bunchPosition);
					model = glm::scale(model, glm::vec3(0.7f));
					renderGrass(shader, model);
				}
			}
		}

	}
}

inline void Entity::setVegetation()
{
	this->vegetation = newVegetation;

	if (first)
	{
		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_real_distribution<float> dist(0, 2);

		for (GLuint i = 0; i < 30; ++i)
		{
			for (int j = 0; j < 30; ++j)
			{
				int bias = dist(mt);
				newVegetation[i][j] += bias;
			}
		}
	}
}
