﻿#pragma once
#include "Includes.h"

class Player
{
public:
	glm::vec3 position;
};

class Camera
{
public:
	enum MovementType
	{
		UNKNOWN,
		FORWARD,
		BACKWARD,
		LEFT,
		RIGHT,
		UP,
		DOWN
	};

private:
	/* Default camera values */
	const float zNEAR = 0.1f;
	const float zFAR = 100.f;
	const float YAW = 0.0f;
	const float PITCH = 2.0f;
	const float FOV = 45.0f;
	glm::vec3 startPosition;

public:
	Camera() = default;
	Camera(const int width, const int height, const glm::vec3 &position);

	void Set(const int width, const int height, const glm::vec3 &position);
	void Reset(const int width, const int height);
	void Reshape(int windowWidth, int windowHeight);

	const glm::mat4 GetViewMatrix() const;
	const glm::vec3 GetPosition() const;
	const glm::mat4 GetProjectionMatrix() const;
	const float GetVelocity(float deltaTime) const;
	const glm::vec3 getPlayerNextPosition(Camera::MovementType direction, float deltaTime);

	void ProcessKeyboard(Camera::MovementType direction, float deltaTime);
	void MouseControl(float xPos, float yPos);
	void ProcessMouseScroll(float yOffset);

	Player player;

	/* Euler Angles */
	float yaw;
	float pitch;

	float moveForward = 0, moveBackward = 0, moveLeft = 0, moveRight = 0;

private:
	void ProcessMouseMovement(float xOffset, float yOffset, bool constrainPitch = true);
	void UpdateCameraVectors();

protected:
	const float cameraSpeedFactor = 2.5f;
	const float mouseSensitivity = 0.1f;

	/* Perspective properties */
	float zNear;
	float zFar;
	float FoVy;
	int width;
	int height;
	bool isPerspective;

	glm::vec3 position;
	glm::vec3 forward;
	glm::vec3 right;
	glm::vec3 up;
	glm::vec3 worldUp;

	bool bFirstMouseMove = true;
	float lastX = 0.f, lastY = 0.f;
};


























//=================================================================================//
//								   FUNCTIONS BODY								   //
//=================================================================================//

Camera::Camera(const int width, const int height, const glm::vec3 & position)
{
	startPosition = position;
	Set(width, height, position);
	player.position = glm::vec3(20.0f, 0.0f, 20.0f);
	FoVy = 90.f;
}

void Camera::Set(const int width, const int height, const glm::vec3 & position)
{
	this->isPerspective = true;
	this->yaw = YAW;
	this->pitch = PITCH;

	this->FoVy = FOV;
	this->width = width;
	this->height = height;
	this->zNear = zNEAR;
	this->zFar = zFAR;

	this->worldUp = glm::vec3(0, 1, 0);
	this->position = position;

	lastX = width / 2.0f;
	lastY = height / 2.0f;
	bFirstMouseMove = true;

	UpdateCameraVectors();
}

void Camera::Reset(const int width, const int height)
{
	Set(width, height, startPosition);
}

void Camera::Reshape(int windowWidth, int windowHeight)
{
	width = windowWidth;
	height = windowHeight;

	/* Define the viewport transformation */
	glViewport(0, 0, windowWidth, windowHeight);
}

const glm::mat4 Camera::GetViewMatrix() const
{
	/* Returns the View Matrix */
	return glm::lookAt(position, position + forward, up);
}

const glm::vec3 Camera::GetPosition() const
{
	return position;
}

const glm::mat4 Camera::GetProjectionMatrix() const
{
	glm::mat4 Proj = glm::mat4(1);
	if (isPerspective) {
		float aspectRatio = ((float)(width)) / height;
		Proj = glm::perspective(glm::radians(FoVy), aspectRatio, zNear, zFar);
	}
	else {
		float scaleFactor = 2000.f;
		Proj = glm::ortho<float>(
			-width / scaleFactor, width / scaleFactor,
			-height / scaleFactor, height / scaleFactor, -zFar, zFar);
	}
	return Proj;
}

inline const float Camera::GetVelocity(float deltaTime) const
{
	return (float)(cameraSpeedFactor * deltaTime);
}

inline const glm::vec3 Camera::getPlayerNextPosition(Camera::MovementType direction, float deltaTime)
{
	glm::vec3 newPosition = player.position;

	float velocity = GetVelocity(deltaTime);
	switch (direction)
	{
	case Camera::MovementType::FORWARD:
		newPosition += forward * velocity;
		newPosition.y = 0;
		break;
	case Camera::MovementType::BACKWARD:
		newPosition -= forward * velocity;
		newPosition.y = 0;
		break;
	case Camera::MovementType::LEFT:
		newPosition -= right * velocity;
		newPosition.y = 0;
		break;
	case Camera::MovementType::RIGHT:
		newPosition += right * velocity;
		newPosition.y = 0;
		break;
	case Camera::MovementType::UP:
		newPosition += up * velocity;
		break;
	case Camera::MovementType::DOWN:
		newPosition -= up * velocity;
		break;
	}
	return newPosition;
}

void Camera::ProcessKeyboard(Camera::MovementType direction, float deltaTime)
{
	moveLeft = 0.0f;
	moveRight = 0.0f;
	float velocity = GetVelocity(deltaTime);
	switch (direction) 
	{
	case Camera::MovementType::FORWARD:
		player.position += forward * velocity;
		player.position.y = 0;
		break;
	case Camera::MovementType::BACKWARD:
		player.position -= forward * velocity;
		player.position.y = 0;
		break;
	case Camera::MovementType::LEFT:
		player.position -= right * velocity;
		player.position.y = 0;
		moveLeft = 1.0f;
		break;
	case Camera::MovementType::RIGHT:
		player.position += right * velocity;
		player.position.y = 0;
		moveRight = 1.0f;
		break;
	case Camera::MovementType::UP:
		player.position.y = 35.0f;
		break;
	case Camera::MovementType::DOWN:
		player.position.y = 0.0f;
		break;
	}
	UpdateCameraVectors();
}

void Camera::MouseControl(float xPos, float yPos)
{
	if (bFirstMouseMove) {
		lastX = xPos;
		lastY = yPos;
		bFirstMouseMove = false;
	}

	float xChange = xPos - lastX;
	float yChange = lastY - yPos;
	lastX = xPos;
	lastY = yPos;

	if (fabs(xChange) <= 1e-6 && fabs(yChange) <= 1e-6) {
		return;
	}
	xChange *= mouseSensitivity;
	yChange *= mouseSensitivity;

	ProcessMouseMovement(xChange, yChange);
}

void Camera::ProcessMouseScroll(float yOffset)
{
	if (FoVy >= 1.0f  && FoVy <= 90.0f) 
	{
		FoVy -= yOffset;
	}
	if (FoVy <= 1.0f)
		FoVy = 1.0f;
	if (FoVy >= 90.0f)
		FoVy = 90.0f;
}

void Camera::ProcessMouseMovement(float xOffset, float yOffset, bool constrainPitch)
{
	yaw += xOffset;
	pitch += yOffset;
	
	//std::cout << "(yaw) x = " << yaw << std::endl;
	//std::cout << "(pitch) y = " << pitch << std::endl;
	//std::cout << std::endl;

	/* Constrain the Range of the Pitch rotation */
	if (constrainPitch) {
		if (pitch > 89.0f)
			pitch = 89.0f;
		if (pitch <= -5.0f)
			pitch = -5.0f;
	}

	// Se modificã vectorii camerei pe baza unghiurilor Euler
	UpdateCameraVectors();
}

void Camera::UpdateCameraVectors()
{
	/* Calculate the new Forward Vector */
	this->forward.x = -cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	this->forward.y = -sin(glm::radians(pitch));
	this->forward.z = -sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	
	/* Normalize the Forward Vector */
	this->forward = glm::normalize(this->forward);

	/* Camera Position */
	this->position.x = (cos(glm::radians(yaw)) * cos(glm::radians(pitch))) + player.position.x;
	this->position.y = sin(glm::radians(pitch)) + player.position.y;
	this->position.z = (sin(glm::radians(yaw)) * cos(glm::radians(pitch))) + player.position.z;

	// Also re-calculate the Right and Up vector
	right = glm::normalize(glm::cross(forward, worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
	up = glm::normalize(glm::cross(right, forward));
}
