#include "Graphic.h"

std::string exePath(char** argv)
{
	std::string strFullExeFileName = argv[0];
	std::string strExePath;
	const size_t last_slash_idx = strFullExeFileName.rfind('\\');
	if (std::string::npos != last_slash_idx) 
	{
		strExePath = strFullExeFileName.substr(0, last_slash_idx);
	}
	return strExePath;
}

int main(int argc, char** argv)
{
	Graphic graphic;

	graphic.draw(exePath(argv));
	
	return 0;
}

